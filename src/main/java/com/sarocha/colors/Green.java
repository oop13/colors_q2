/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sarocha.colors;

/**
 *
 * @author Sarocha
 */
public class Green extends Colors {

    private char blue;
    private char yellow;

    public Green(char blue, char yellow) {
        this.blue = blue;
        this.yellow = yellow;
    }

    @Override
    public void mixed() {
        System.out.println("Green color " + "--> (blue + yellow)");
    }

}
