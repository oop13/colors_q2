/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sarocha.colors;

/**
 *
 * @author Sarocha
 */
public class MainProgram {

    public static void main(String[] args) {
        Orange orange1 = new Orange('r', 'y');
        orange1.mixed("Orange");
        orange1.mixed();
        space();
        Green green1 = new Green('b', 'y');
        green1.mixed("Green");
        green1.mixed();
        space();
        Pink pink1 = new Pink('r', 'w');
        pink1.mixed("Pink");
        pink1.mixed();
        space();
        Purple purple1 = new Purple('r', 'b');
        purple1.mixed("Purple");
        purple1.mixed();
        space();
        Violet violet1 = new Violet('r', 'p');
        violet1.mixed("Violet");
        violet1.mixed();
    }

    public static void space() {
        System.out.println();
    }

}
