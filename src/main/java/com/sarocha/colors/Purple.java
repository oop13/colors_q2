/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sarocha.colors;

/**
 *
 * @author Sarocha
 */
public class Purple extends Colors {

    private char red;
    private char blue;

    public Purple(char red, char blue) {
        this.red = red;
        this.blue = blue;
    }

    @Override
    public void mixed() {
        System.out.println("Purple color " + "--> (red + blue)");
    }

}
