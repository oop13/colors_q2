/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sarocha.colors;

/**
 *
 * @author Sarocha
 */
public class Violet extends Colors {

    private char red;
    private char purple;

    public Violet(char red, char purple) {
        this.red = red;
        this.purple = purple;
    }

    @Override
    public void mixed() {
        System.out.println("Violet color " + "--> (red + purple)");
    }

}
