/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sarocha.colors;

/**
 *
 * @author Sarocha
 */
public class Orange extends Colors {
    private char red;
    private char yellow;
    
    public Orange (char red, char yellow) {
        this.red = red;
        this.yellow = yellow;
    }
    
    @Override
    public void mixed() {
        System.out.println("Orange color " + "--> (red + yellow)");
    }

}
