/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sarocha.colors;

/**
 *
 * @author Sarocha
 */
public class Pink extends Colors {

    private char red;
    private char white;

    public Pink(char red, char white) {
        this.red = red;
        this.white = white;
    }

    @Override
    public void mixed() {
        System.out.println("Pink color " + "--> (red + white)");
    }

}
